package de.tudresden.inf.st.pnml.gen.generators;

import de.tudresden.inf.st.pnml.gen.data.*;

import java.util.HashMap;
import java.util.Map;

public class JavaClassGenUtil extends AbstractGenUtil{

    public static String generateLauncherClass(String rosHost, String rosMasterUri, String signalHost,
                                          String packageNamespace, String netPath, String nodeName){

        Map<String, Object> context = new HashMap<>();
        LauncherContext launcherContext = new LauncherContext(rosHost, rosMasterUri, signalHost,
                packageNamespace, netPath, nodeName);
        context.put("launcherContext", launcherContext);
        return generate(context, "JavaLauncher.mustache");
    }

    public static String generateTracingLauncherClass(String rosHost, String rosMasterUri, String signalHost,
                                               String packageNamespace){

        Map<String, Object> context = new HashMap<>();
        LauncherContext launcherContext = new LauncherContext(rosHost, rosMasterUri, signalHost,
                packageNamespace, null, null);
        context.put("launcherContext", launcherContext);
        return generate(context, "JavaTracingLauncher.mustache");
    }

    public static String generateNodeClass(String packageNamespace, String nodeName, String protocol){

        Map<String, Object> context = new HashMap<>();
        NodeContext nodeContext = new NodeContext(packageNamespace, nodeName, protocol);
        context.put("nodeContext", nodeContext);
        return generate(context, "JavaNode.mustache");
    }
}
