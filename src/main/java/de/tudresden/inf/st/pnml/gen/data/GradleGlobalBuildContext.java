package de.tudresden.inf.st.pnml.gen.data;

public class GradleGlobalBuildContext {

    private String archiveBaseName;
    private String version;
    private String pnmlRelastVersion;
    private String group;

    public GradleGlobalBuildContext(String archiveBaseName, String version,
                                    String pnmlRelastVersion, String group) {
        this.archiveBaseName = archiveBaseName;
        this.version = version;
        this.pnmlRelastVersion = pnmlRelastVersion;
        this.group = group;
    }

    public String getArchiveBaseName() {
        return archiveBaseName;
    }

    public void setArchiveBaseName(String archiveBaseName) {
        this.archiveBaseName = archiveBaseName;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getPnmlRelastVersion() {
        return pnmlRelastVersion;
    }

    public void setPnmlRelastVersion(String pnmlRelastVersion) {
        this.pnmlRelastVersion = pnmlRelastVersion;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
