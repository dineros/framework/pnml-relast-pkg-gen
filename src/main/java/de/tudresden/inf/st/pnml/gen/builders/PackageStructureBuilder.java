package de.tudresden.inf.st.pnml.gen.builders;

import de.tudresden.inf.st.pnml.gen.json.NetInfo;
import de.tudresden.inf.st.pnml.jastadd.model.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

public class PackageStructureBuilder {

    private static final String FILE_PATH = "../pnml-relast-pkg-gen/src/main/resources/static/";

    public static void build(String packagePath, DinerosPackage dinerosPackage, List<NetInfo> netInfos, String engineVersion,
                             String rosHost, String rosMasterUri, String signalHost, String protocol) {

        List<PetriNet> nets = new ArrayList<>();

        for (NetInfo n : netInfos) {
            nets.add(PnmlParser.parsePnml(n.net, false).get(0));
        }

        try {
            Files.createDirectories(Paths.get(packagePath));

            buildDinerosPackageStructure(dinerosPackage.getRootName(), dinerosPackage.getNamespace(), dinerosPackage);
            buildDinerosGeneratedFilesStructure(dinerosPackage, nets, netInfos);
            buildStaticFilesIntoDinerosPackage(dinerosPackage, engineVersion);

            includeDirectoriesRecursive(dinerosPackage.getDir(0), packagePath);
            includeTemplateFiles(dinerosPackage, protocol, netInfos, rosHost, rosMasterUri, signalHost, engineVersion);
            includeFilesInPackageRecursive(dinerosPackage.getDir(0), packagePath);

            insertStringsAsFilesRecursive(dinerosPackage.getDir(0), packagePath);

        } catch (IOException e) {
            System.err.println(e.getLocalizedMessage() + " caused by: " + e.getCause());
            e.printStackTrace();

        }
    }

    /////////////////////////////////////
    // STRING TO FILE ///////////////////
    /////////////////////////////////////

    private static void insertStringsAsFilesRecursive(PackageDirectory packageDirectory, String packagePath) {

        for (PackageFile pf : packageDirectory.getFileList()){
            if(pf.isTemplateFile()) {

                if(pf.isJavaFile()){
                    System.out.println("Writing file " + pf.getName() +  " to: " + packagePath + "/" + packageDirectory.getName() + "/" + pf.getName() + ".java");
                    writeToFile(packagePath + "/" + packageDirectory.getName() + "/" + pf.getName() + ".java", pf.asTemplateFile().getFileContent());
                }else {
                    System.out.println("Writing file " + pf.getName() +  " to: " + packagePath + "/" + packageDirectory.getName() + "/" + pf.getName());
                    writeToFile(packagePath + "/" + packageDirectory.getName() + "/" + pf.getName(), pf.asTemplateFile().getFileContent());
                }
            }
        }

        for (PackageDirectory d : packageDirectory.getDirList()) {
            insertStringsAsFilesRecursive(d, packagePath + "/" + packageDirectory.getName());
        }
    }

    private static void writeToFile(String path, String content){

        try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), StandardCharsets.UTF_8))) {
            writer.write(content);
        }
        catch (IOException ex) {
            System.err.println("ERROR: " + ex.getMessage());
            ex.printStackTrace();
        }
    }

    /////////////////////////////////////
    // TREE GEN /////////////////////////
    /////////////////////////////////////

    private static void buildDinerosGeneratedFilesStructure(DinerosPackage dinerosPackage, List<PetriNet> nets, List<NetInfo> netInfos) {

        for (PackageDirectory pd : dinerosPackage.allDirectories()) {

            if (pd.getName().equals(dinerosPackage.getRootName())) {
                GradleFile buildGradle = new GradleFile();
                buildGradle.setName("build.gradle");
                buildGradle.setFileID(UUID.randomUUID().toString());
                pd.addFile(buildGradle);

                GradleFile gradleProbs = new GradleFile();
                gradleProbs.setName("gradle.properties");
                gradleProbs.setFileID(UUID.randomUUID().toString());
                pd.addFile(gradleProbs);

                GradleFile gradleSettings = new GradleFile();
                gradleSettings.setName("settings.gradle");
                gradleSettings.setFileID(UUID.randomUUID().toString());
                pd.addFile(gradleSettings);
            }

            if (pd.getName().equals("nodes")) {
                for (NetInfo netInfo : netInfos) {
                    NodeFile nodeFile = new NodeFile();
                    nodeFile.setName(netInfo.name);
                    nodeFile.setFileID(UUID.randomUUID().toString());
                    pd.addFile(nodeFile);
                }
            }

            if (pd.getName().equals("launchers")) {
                for (NetInfo netInfo : netInfos) {
                    LauncherFile launcher = new LauncherFile();
                    launcher.setName(netInfo.name + "Launcher");
                    launcher.setFileID(UUID.randomUUID().toString());
                    pd.addFile(launcher);
                }

                TracingFile tracingFile = new TracingFile();
                tracingFile.setName("TracingNodeLauncher");
                tracingFile.setFileID(UUID.randomUUID().toString());
                pd.addFile(tracingFile);
            }

            if(pd.getName().equals("nets")){
                for(int i = 0; i < nets.size(); i++){
                    PnmlFile pnmlFile = new PnmlFile();
                    pnmlFile.setName("PetriNet-" + i + ".pnml");
                    pnmlFile.setFileID(UUID.randomUUID().toString());
                    pnmlFile.setSourcePath(netInfos.get(i).net);
                    String[] splitSourcePath = netInfos.get(i).net.split("/");
                    pnmlFile.setSourceName(splitSourcePath[splitSourcePath.length - 1]);
                    pd.addFile(pnmlFile);
                }
            }
        }
    }

    private static void buildStaticFilesIntoDinerosPackage(DinerosPackage dinerosPackage, String engineVersion) {

        for (PackageDirectory pd : dinerosPackage.allDirectories()) {

            if (pd.getName().equals("wrapper")) {
                StaticFile wrapperJar = new StaticFile();
                wrapperJar.setName("gradle-wrapper.jar");
                wrapperJar.setFileID(UUID.randomUUID().toString());
                pd.addFile(wrapperJar);

                StaticFile wrapperProperties = new StaticFile();
                wrapperProperties.setName("gradle-wrapper.properties");
                wrapperProperties.setFileID(UUID.randomUUID().toString());
                pd.addFile(wrapperProperties);
            }

            if (pd.getName().equals(dinerosPackage.getRootName())) {
                StaticFile gradlew = new StaticFile();
                gradlew.setName("gradlew");
                gradlew.setFileID(UUID.randomUUID().toString());
                pd.addFile(gradlew);

                StaticFile gradlewBat = new StaticFile();
                gradlewBat.setName("gradlew.bat");
                gradlewBat.setFileID(UUID.randomUUID().toString());
                pd.addFile(gradlewBat);
            }

            if (pd.getName().equals("libs")) {
                StaticFile engineJar = new StaticFile();
                engineJar.setName("pnml-relast-engine-fatjar-" + engineVersion + ".jar");
                engineJar.setFileID(UUID.randomUUID().toString());
                pd.addFile(engineJar);
            }
        }
    }

    private static void buildDinerosPackageStructure(String rootName, String namespace, DinerosPackage dp) {
        PackageDirectory root = new PackageDirectory();
        root.setName(rootName);
        dp.addDir(root);

        PackageDirectory libs = new PackageDirectory();
        libs.setName("libs");
        root.addDir(libs);

        PackageDirectory src = new PackageDirectory();
        src.setName("src");
        root.addDir(src);

        PackageDirectory main = new PackageDirectory();
        main.setName("main");
        src.addDir(main);

        PackageDirectory resources = new PackageDirectory();
        resources.setName("resources");
        main.addDir(resources);

        PackageDirectory java = new PackageDirectory();
        java.setName("java");
        main.addDir(java);

        List<PackageDirectory> packagePDs = new ArrayList<>();

        for (String s : namespace.split("\\.")) {
            PackageDirectory pd = new PackageDirectory();
            pd.setName(s);
            packagePDs.add(pd);
        }

        java.addDir(packagePDs.get(0));

        for(int i = 1; i < packagePDs.size(); i++){
            packagePDs.get(i-1).addDir(packagePDs.get(i));
        }

        PackageDirectory nodes = new PackageDirectory();
        nodes.setName("nodes");
        packagePDs.get(packagePDs.size()-1).addDir(nodes);

        PackageDirectory launchers = new PackageDirectory();
        launchers.setName("launchers");
        packagePDs.get(packagePDs.size()-1).addDir(launchers);

        PackageDirectory handlers = new PackageDirectory();
        handlers.setName("handlers");
        packagePDs.get(packagePDs.size()-1).addDir(handlers);

        PackageDirectory nets = new PackageDirectory();
        nets.setName("nets");
        resources.addDir(nets);

        PackageDirectory gradle = new PackageDirectory();
        gradle.setName("gradle");
        root.addDir(gradle);

        PackageDirectory wrapper = new PackageDirectory();
        wrapper.setName("wrapper");
        gradle.addDir(wrapper);
    }

    /////////////////////////////////////
    // TREE TO FILE TREE ////////////////
    /////////////////////////////////////

    private static void includeTemplateFiles(DinerosPackage dinerosPackage, String protocol, List<NetInfo> netInfos,
                                             String rosHost, String rosMasterUri, String signalHost, String engineVersion) {

        System.out.println("Generating contents of nodes.");
        for (NetInfo netInfo : netInfos) {
            for (PackageFile pf : dinerosPackage.allPackageFiles()) {
                if (pf.isNodeFile() && pf.getName().equals(netInfo.name)){
                    pf.setName(netInfo.name);
                    pf.asNodeFile().generate(protocol);
                }
            }
        }

        System.out.println("Generating contents of launchers.");
        for (NetInfo netInfo : netInfos) {
            for (PackageFile pf : dinerosPackage.allPackageFiles()) {
                String launchFileName = netInfo.name + "Launcher";
                if (pf.isLauncherFile() && pf.getName().equals(launchFileName)){
                    pf.setName(launchFileName);
                    pf.asLauncherFile().generate(rosHost, rosMasterUri, signalHost, netInfo.net, netInfo.name);
                }
            }
        }

        System.out.println("Generating contents of tracing launcher.");
        for (PackageFile pf : dinerosPackage.allPackageFiles()) {
            if(pf.isTracingFile()){
                pf.asTracingFile().generate(rosHost, rosMasterUri, signalHost);
            }
        }

        System.out.println("Generating contents of gradle files.");
        for (PackageFile pf : dinerosPackage.allPackageFiles()) {

            if (pf.getName().equals("build.gradle")) {
                System.out.println(">>> build.gradle");
                pf.asGradleFile().generateBuildGradle(dinerosPackage.getRootName(), dinerosPackage.getVersion(),
                        engineVersion, dinerosPackage.getNamespace(), netInfos);

            }

            if (pf.getName().equals("settings.gradle")) {
                System.out.println(">>> settings.gradle");
                pf.asGradleFile().generateSettingsGradle(dinerosPackage.getRootName());
            }
        }
    }

    private static void includeDirectoriesRecursive(PackageDirectory packageDirectory, String packagePath) throws IOException {

        for (PackageDirectory d : packageDirectory.getDirList()) {
            includeDirectoriesRecursive(d, packagePath + "/" + packageDirectory.getName());
        }

        if (packageDirectory.getNumDir() == 0) {
            Files.createDirectories(Paths.get(packagePath + "/" + packageDirectory.getName()));
        }
    }

    private static void includeFilesInPackageRecursive(PackageDirectory packageDirectory, String packagePath) throws IOException {

        for (PackageDirectory d : packageDirectory.getDirList()) {
            includeFilesInPackageRecursive(d, packagePath + "/" + packageDirectory.getName());
        }

        if (packageDirectory.getNumFile() > 0) {
            for (PackageFile pf : packageDirectory.getFileList()) {
                if (pf.isStaticFile()) {

                    Path copied;

                    if(pf.isPnmlFile()){
                        copied = Paths.get(packagePath + "/" + packageDirectory.getName() + "/" + pf.asPnmlFile().getSourceName());
                    }else {
                        copied = Paths.get(packagePath + "/" + packageDirectory.getName() + "/" + pf.getName());
                    }

                    Path originalPath;

                    if (pf.getName().contains(".java")) {
                        originalPath = Paths.get(FILE_PATH + "/" + pf.getName().split("\\.")[0]);
                    } else if (pf.isPnmlFile()) {
                        originalPath = Paths.get(pf.asPnmlFile().getSourcePath());
                    } else {
                        originalPath = Paths.get(FILE_PATH + "/" + pf.getName());
                    }

                    Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
                    System.out.println("Copy:" + originalPath + " --> " + copied);
                }
            }
        }
    }
}