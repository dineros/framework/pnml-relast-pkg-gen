package de.tudresden.inf.st.pnml.gen.data;

public class NodeContext {

    private String packageNamespace;
    private String nodeClassName;
    private String signalProtocol;

    public NodeContext(String packageNamespace, String nodeClassName, String signalProtocol) {
        this.packageNamespace = packageNamespace;
        this.nodeClassName = nodeClassName;
        this.signalProtocol = signalProtocol;
    }

    public String getPackageNamespace() {
        return packageNamespace;
    }

    public void setPackageNamespace(String packageNamespace) {
        this.packageNamespace = packageNamespace;
    }

    public String getNodeClassName() {
        return nodeClassName;
    }

    public void setNodeClassName(String nodeClassName) {
        this.nodeClassName = nodeClassName;
    }

    public String getSignalProtocol() {
        return signalProtocol;
    }

    public void setSignalProtocol(String signalProtocol) {
        this.signalProtocol = signalProtocol;
    }
}
