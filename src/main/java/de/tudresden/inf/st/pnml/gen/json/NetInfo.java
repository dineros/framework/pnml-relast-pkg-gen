package de.tudresden.inf.st.pnml.gen.json;

public class NetInfo {

    public String net;
    public String name;
    public String rosHost;
    public String rosMasterUri;
    public String signalHost;
}
