package de.tudresden.inf.st.pnml.gen.json;

public class GeneratorConfig {

    public String packageName;
    public String rootName;
    public String namespace;
    public String version;
    public String configFile;
    public String rosHost;
    public String rosMasterUri;
    public String signalHost;
    public String signalProtocol;
    public String targetPath;

    public GeneratorConfig(String packageName, String rootName, String namespace,
                           String version, String configFile, String rosHost,
                           String rosMasterUri, String signalProtocol, String signalHost, String targetPath) {
        this.packageName = packageName;
        this.rootName = rootName;
        this.namespace = namespace;
        this.version = version;
        this.configFile = configFile;
        this.rosHost = rosHost;
        this.rosMasterUri = rosMasterUri;
        this.signalHost = signalHost;
        this.targetPath = targetPath;
        this.signalProtocol = signalProtocol;
    }

    public GeneratorConfig(){}
}
