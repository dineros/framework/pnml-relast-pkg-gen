package de.tudresden.inf.st.pnml.gen.generators;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

public class AbstractGenUtil {

    protected static final String TEMPLATE_PATH = "../pnml-relast-pkg-gen/src/main/resources/mustache/";

    protected static String generate(Map<String, Object> context, String fileName) {
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache m = mf.compile(TEMPLATE_PATH + fileName);

        StringWriter writer = new StringWriter();
        try {
            m.execute(writer, context).flush();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return writer.toString();
    }
}
