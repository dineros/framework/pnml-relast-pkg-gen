package de.tudresden.inf.st.pnml.gen.generators;

import de.tudresden.inf.st.pnml.gen.data.GradleGlobalBuildContext;
import de.tudresden.inf.st.pnml.gen.data.GradleTaskBuildContext;
import de.tudresden.inf.st.pnml.gen.data.SettingsGradleContext;
import de.tudresden.inf.st.pnml.gen.json.NetInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GradleGenUtil extends AbstractGenUtil{

    public static String generateBuildGradle(String archiveBaseName, String version,
                                             String pnmlRelastVersion, String group,
                                             List<NetInfo> netInfos){

        GradleGlobalBuildContext gradleGlobalBuildContext =
                new GradleGlobalBuildContext(archiveBaseName, version, pnmlRelastVersion, group);
        Map<String, Object> context = new HashMap<>();
        context.put("gradleGlobalBuildContext", gradleGlobalBuildContext);

        List<GradleTaskBuildContext> tasks = new ArrayList<>();

        for(NetInfo n : netInfos){
            tasks.add(new GradleTaskBuildContext(n.name + "Launcher", archiveBaseName + "-" + n.name,
                    n.name, group));
        }
        context.put("tasks", tasks);

        return generate(context, "build.mustache");
    }

    public static String generateGradleSettings(String rootProjectName){

        SettingsGradleContext settingsGradleContext = new SettingsGradleContext(rootProjectName);
        Map<String, Object> context = new HashMap<>();
        context.put("settingsGradleContext", settingsGradleContext);

        return generate(context, "settings.mustache");
    }
}
