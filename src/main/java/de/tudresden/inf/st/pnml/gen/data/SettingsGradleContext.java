package de.tudresden.inf.st.pnml.gen.data;

public class SettingsGradleContext {

    private String rootProjectName;

    public String getRootProjectName() {
        return rootProjectName;
    }

    public void setRootProjectName(String rootProjectName) {
        this.rootProjectName = rootProjectName;
    }

    public SettingsGradleContext(String rootProjectName) {
        this.rootProjectName = rootProjectName;
    }
}
