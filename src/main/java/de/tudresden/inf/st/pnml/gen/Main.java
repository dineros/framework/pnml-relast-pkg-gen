package de.tudresden.inf.st.pnml.gen;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.tudresden.inf.st.pnml.gen.builders.PackageStructureBuilder;
import de.tudresden.inf.st.pnml.gen.json.GeneratorConfig;
import de.tudresden.inf.st.pnml.gen.json.NetInfo;
import de.tudresden.inf.st.pnml.jastadd.model.DinerosPackage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Main {

    static final String ENGINE_VERSION = "0.1";

    public static void main(java.lang.String[] args) throws IOException {

        if(args.length != 1){
            System.out.println("Error: Invalid arguments.");
            return;
        }

        ObjectMapper generatorConfigObjectMapper = new ObjectMapper();
        GeneratorConfig gc = generatorConfigObjectMapper.readValue(Files.readString(Paths.get(args[0])), GeneratorConfig.class);
        /*GeneratorConfig gc = new GeneratorConfig("sample", "sample-pkg",
                "de.tudresden.inf.st.sorting","0.1",
                "src/test/resources/EngineConfig.json","mqtt", "localhost",
                "http://localhost:11311","localhost",
                "/home/sebastian/Desktop/tmp");*/

        DinerosPackage dp = new DinerosPackage();
        dp.setName(gc.packageName);
        dp.setRootName(gc.rootName);
        dp.setNamespace(gc.namespace);
        dp.setVersion(gc.version);

        String configString = Files.readString(Paths.get(gc.configFile));
        ObjectMapper netObjectMapper = new ObjectMapper();
        List<NetInfo> netInfos = Arrays.asList(netObjectMapper.readValue(configString, NetInfo[].class));

        PackageStructureBuilder.build(gc.targetPath, dp, netInfos, ENGINE_VERSION
                , gc.rosHost, gc.rosMasterUri, gc.signalHost, gc.signalProtocol);
    }
}
