package de.tudresden.inf.st.pnml.gen.data;

public class LauncherContext {

    private String rosHost;
    private String rosMasterUri;
    private String signalHost;
    private String packageNamespace;
    private String netPath;
    private String nodeClassName;

    public LauncherContext(String rosHost, String rosMasterUri, String signalHost,
                           String packageNamespace, String netPath, String nodeClassName) {
        this.rosHost = rosHost;
        this.rosMasterUri = rosMasterUri;
        this.signalHost = signalHost;
        this.packageNamespace = packageNamespace;
        this.netPath = netPath;
        this.nodeClassName = nodeClassName;
    }

    public String getRosHost() {
        return rosHost;
    }

    public void setRosHost(String rosHost) {
        this.rosHost = rosHost;
    }

    public String getRosMasterUri() {
        return rosMasterUri;
    }

    public void setRosMasterUri(String rosMasterUri) {
        this.rosMasterUri = rosMasterUri;
    }

    public String getSignalHost() {
        return signalHost;
    }

    public void setSignalHost(String signalHost) {
        this.signalHost = signalHost;
    }

    public String getPackageNamespace() {
        return packageNamespace;
    }

    public void setPackageNamespace(String packageNamespace) {
        this.packageNamespace = packageNamespace;
    }

    public String getNetPath() {
        return netPath;
    }

    public void setNetPath(String netPath) {
        this.netPath = netPath;
    }

    public String getNodeClassName() {
        return nodeClassName;
    }

    public void setNodeClassName(String nodeName) {
        this.nodeClassName = nodeName;
    }
}
