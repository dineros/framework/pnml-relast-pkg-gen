package de.tudresden.inf.st.pnml.gen.data;

public class GradleTaskBuildContext {

    private String mainClass;
    private String archiveBaseName;
    private String nodeName;
    private String packageNamespace;

    public GradleTaskBuildContext(String mainClass, String archiveBaseName, String nodeName, String packageNamespace) {
        this.mainClass = mainClass;
        this.archiveBaseName = archiveBaseName;
        this.nodeName = nodeName;
        this.packageNamespace = packageNamespace;
    }

    public String getMainClass() {
        return mainClass;
    }

    public void setMainClass(String mainClass) {
        this.mainClass = mainClass;
    }

    public String getArchiveBaseName() {
        return archiveBaseName;
    }

    public void setArchiveBaseName(String archiveBaseName) {
        this.archiveBaseName = archiveBaseName;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getPackageNamespace() {
        return packageNamespace;
    }

    public void setPackageNamespace(String packageNamespace) {
        this.packageNamespace = packageNamespace;
    }
}
